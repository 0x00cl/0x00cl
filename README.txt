Hello! I'm Tomás Gutiérrez L. (0x00)
------------------------------------

I studied Computer Science in _Universidad de Santiago de Chile_ (USACH) and 
I enjoy learning new tools and technologies. My foucs right now are things 
related to back-end and DevOps.

I like to be minimalistic design and code wise, as you might see by the README 
that is not written in markdown and also web projects I try to make the design 
minimalistic.

--------------------------------------------------------------------------------

My skills:

* Programming Languages
    * Python
        * Flask
        * SQLAlchemy
    * Bash
    * Go
* Technologies
    * Jenkins
    * Docker
    * Prometheus
    * Ansible
    * Logstash
    * Git
    * Linux

Check out my blog at https://0x00.cl